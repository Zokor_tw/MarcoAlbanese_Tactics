﻿using System.Collections;
using UnityEngine;
using System;


public class TileMap : Singleton<TileMap> {

    public TileType[] tileTypes;

    public int[,] tiles;

    public int mapSizeX = 10;
    public int mapSizeY = 10;

    void Start()
    {
        tiles = new int[mapSizeX, mapSizeY];
        int x, y;
        for (x = 0; x < mapSizeX; x++)
        {
            for (y = 0; y < mapSizeY; y++)
            {
                tiles[x, y] = UnityEngine.Random.Range(0,3);
            }
        }



        GenerateMapVisual();

       

    }

  
    void GenerateMapVisual()
    {
        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                TileType tt = tileTypes[tiles[x, y]];
                Instantiate(tt.tileVisualPrefab, new Vector3(x, 0, y), Quaternion.identity);
            }
        }
    }
	
}
