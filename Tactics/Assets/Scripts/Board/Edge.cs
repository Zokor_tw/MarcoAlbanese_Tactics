﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Edge<T> where T:IComparable {

    public int weight { get; private set; }
    public Node <T> start { get; private set; }
    public Node <T> end { get; private set; }

    public Edge (Node<T> _start,Node<T> _end,int weight)
    {
        this.start = _start;
        this.end = _end;
        this.weight = weight;
    }

}
