﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class Graph<T> where T:IComparable
{
    List<Node<T>> nodes;
    Dictionary<T,Node<T>> nodesDictionary;
    List<Edge<T>> edges;

    public Graph()
    {
        nodes = new List<Node<T>>();
        nodesDictionary = new Dictionary<T, Node<T>>();
        edges = new List<Edge<T>>();
    }
    public void AddEdges(Node<T> start, Node<T> end, int weight)
    {
        if (this.GetEdge(start, end, weight) == default(Edge<T>))
        {
            edges.Add(new Edge<T>(start, end, weight));
        }
        else
        {
            return;
        }
    }
    public void AddNode(T item)
    {
        if (!nodesDictionary.ContainsKey(item))
        {
            nodes.Add(new Node<T>(item));
            nodesDictionary.Add(item, nodes.Last());
        }
    }

    public Edge<T> GetEdge(Node<T> start, Node<T> end, int weight)
    {
        Edge<T> edg = default(Edge<T>);
        foreach (Edge<T> e in edges)
        {
            if (e.start == start && e.end == end && e.weight == weight)
            {
                edg = e;
            }
        }
        return edg;
    }

}
