﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Node<T> where T:IComparable {

    public T item { get; private set; }
    public List<Edge<T>> edges;

    public Node(T item)
    {
        this.item = item;
    }
}
